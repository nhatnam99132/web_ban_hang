<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function login(Request $request) {
        $credentials = $request->only('email', 'password');
        if(Auth::guard('admin')->attempt($credentials)) {
            return redirect()->route('admin.dashboard');
        } else {
            echo "Đăng nhập thất bại";
        }

    }
    public function dashboard() {
        $admin = Auth::guard('admin')->user();
        return view('admin.dashboard', ['user' => $admin]);
    }
    public function logout() {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
    public function statistics() {
        $admin = Auth::guard('admin')->user();
        return view('admin.statistics', ['user' => $admin]);
    }
}
