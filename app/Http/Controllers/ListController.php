<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ListController extends Controller
{
    //
    public function index(Request $request, $model) {
        $admin = Auth::guard('admin')->user();
        return view('admin.list', ['user' => $admin]);
    }
}
