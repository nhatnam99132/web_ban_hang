<?php

namespace App\Http\Livewire;
use App\Models\Coupon;
use Livewire\Component;

class AdminCouponsComponent extends Component
{
    public function deleteCoupon($id)
    {
        $coupon = Coupon::find($id);
        $coupon->delete();
        session()->flash('message', 'Mã giảm giá đã được xóa thành công');
    }

    public function render()
    {
        $coupons = Coupon::all();
        return view('livewire.admin-coupons-component', ['coupons' => $coupons])->layout('layouts.base');
    }
}
