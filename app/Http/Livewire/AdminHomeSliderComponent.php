<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\HomeSlider;
use Livewire\WithPagination;
class AdminHomeSliderComponent extends Component
{
    use WithPagination;

    public function deleteSlide($slide_id)
    {
        $product = HomeSlider::find($slide_id);
        $product->delete();
        session()->flash('message', 'Slide đã được xóa thành công');
    }

    public function render()
    {
        $sliders = HomeSlider::paginate(10);
        return view('livewire.admin-home-slider-component', ['sliders' => $sliders])->layout('layouts.base');
    }
}
