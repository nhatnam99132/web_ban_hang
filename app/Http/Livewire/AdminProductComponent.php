<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Product;
use Livewire\WithPagination;
class AdminProductComponent extends Component
{
    use WithPagination;

    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $product->delete();
        session()->flash('message', 'Sản phẩm đã được xóa thành công');
    }

    public function render()
    {
        $products = Product::paginate(10);
        return view('livewire.admin-product-component', ['products' => $products])->layout('layouts.base');
    }
}
