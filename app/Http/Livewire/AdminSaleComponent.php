<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Sale;
class AdminSaleComponent extends Component
{
    public $sale_date;
    public $status;

    public function mount()
    {
        $sale = Sale::find(1);
        $this->sale_date = $sale->sale_date;
        $this->status = $sale->status;
    }

    public function updateSale()
    {
        $sale = Sale::find(1);
        $sale->sale_date = $this->sale_date;
        $sale->status = $this->status;
        $sale->save();
        session()->flash('message', 'Khuyến mãi đã được cập nhật thành công');
    }

    public function render()
    {
        return view('livewire.admin-sale-component')->layout('layouts.base');
    }
}
