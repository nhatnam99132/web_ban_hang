<div class="navbar nav_title" style="border: 0;">
    <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
  </div>

  <div class="clearfix"></div>

  <!-- menu profile quick info -->
  <div class="profile clearfix">
    <div class="profile_pic">
      <img src="https://scontent.fdad3-1.fna.fbcdn.net/v/t1.0-1/cp0/p80x80/140327358_1589119524605444_5152023498256716138_o.jpg?_nc_cat=102&ccb=3&_nc_sid=dbb9e7&_nc_ohc=f3e5JAPtbKIAX9ZNPAj&_nc_ht=scontent.fdad3-1.fna&tp=27&oh=004711425bf8f714bf51aeb69afe036d&oe=6066B494" alt="..." class="img-circle profile_img">
    </div>
    <div class="profile_info">
      <span>Welcome,</span>
      <h2><?=$user->name?></h2>
    </div>
  </div>
  <!-- /menu profile quick info -->

  <br />

  <!-- sidebar menu -->
  <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{route('list.index', ['model'=>'category'])}}">Danh mục</a></li>
            <li><a href="{{route('list.index', ['model'=>'news'])}}">Tin tức</a></li>
            <li><a href="{{route('list.index', ['model'=>'product'])}}">Sản phẩm</a></li>
            <li><a href="{{route('list.index', ['model'=>'order'])}}">Đơn hàng</a></li>
            <li><a href="{{route('list.index', ['model'=>'user'])}}">Thành viên</a></li>
          </ul>
        </li>
        </ul>
    </div>

  </div>
  <!-- /sidebar menu -->

  <!-- /menu footer buttons -->
  <div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
  </div>