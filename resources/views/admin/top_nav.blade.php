<div class="nav_menu">
    <div class="nav toggle">
      <a id="menu_toggle"><i class="fa fa-bars"></i></a>
    </div>
    <nav class="nav navbar-nav">
    <ul class=" navbar-right">
      <li class="nav-item dropdown open" style="padding-left: 15px;">
        <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
          <img src="https://scontent.fdad3-1.fna.fbcdn.net/v/t1.0-1/cp0/p80x80/140327358_1589119524605444_5152023498256716138_o.jpg?_nc_cat=102&ccb=3&_nc_sid=dbb9e7&_nc_ohc=f3e5JAPtbKIAX9ZNPAj&_nc_ht=scontent.fdad3-1.fna&tp=27&oh=004711425bf8f714bf51aeb69afe036d&oe=6066B494" alt=""><?=$user->name?>
        </a>
        <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item"  href="javascript:;"> Profile</a>
            <a class="dropdown-item"  href="javascript:;">
              <span class="badge bg-red pull-right">50%</span>
              <span>Settings</span>
            </a>
        <a class="dropdown-item"  href="javascript:;">Help</a>
          <a class="dropdown-item"  href="{{route('admin.logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
        </div>
      </li>

      <li role="presentation" class="nav-item dropdown open">
        <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
          <i class="fa fa-envelope-o"></i>
          <span class="badge bg-green">6</span>
        </a>
        <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
          <li class="nav-item">
            <a class="dropdown-item">
              <span class="image"><img src="#" alt="Profile Image" /></span>
              <span>
                <span>John Smith</span>
                <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                Film festivals used to be do-or-die moments for movie makers. They were where...
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="dropdown-item">
              <span class="image"><img src="#" alt="Profile Image" /></span>
              <span>
                <span>John Smith</span>
                <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                Film festivals used to be do-or-die moments for movie makers. They were where...
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="dropdown-item">
              <span class="image"><img src="#" alt="Profile Image" /></span>
              <span>
                <span>John Smith</span>
                <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                Film festivals used to be do-or-die moments for movie makers. They were where...
              </span>
            </a>
          </li>
          <li class="nav-item">
            <a class="dropdown-item">
              <span class="image"><img src="#" alt="Profile Image" /></span>
              <span>
                <span>John Smith</span>
                <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                Film festivals used to be do-or-die moments for movie makers. They were where...
              </span>
            </a>
          </li>
          <li class="nav-item">
            <div class="text-center">
              <a class="dropdown-item">
                <strong>See All Alerts</strong>
                <i class="fa fa-angle-right"></i>
              </a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </nav>
</div>