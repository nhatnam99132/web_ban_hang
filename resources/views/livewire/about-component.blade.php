<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="#" class="link">home</a></li>
                <li class="item-link"><span>Contact us</span></li>
            </ul>
        </div>
    </div>
    
    <div class="container">
        <!-- <div class="main-content-area"> -->
            <div class="aboutus-info style-center">
                <b class="box-title">Thông tin thú vị về chúng tối</b>
                <p class="txt-content">Ra mắt năm 2021, nền tảng thương mại Hublop được xây dựng nhằm cung cấp cho người sử dùng những trải nghiệm dễ dàng, an toàn và nhanh chóng khi mua sắm trực tuyến thông qua hệ thống hỗ trợ thanh toán và vận hành vững mạnh.

                    Chúng tôi có niềm tin mạnh mẽ rằng trải nghiệm mua sắm trực tuyến phải đơn giản, dễ dàng và mang đến cảm xúc vui thích. Niềm tin này truyền cảm hứng và thúc đẩy chúng tôi mỗi ngày tại Hublop.</p>
            </div>

            <div class="row equal-container">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="aboutus-box-score equal-elem ">
                        <b class="box-score-title">100%</b>
                        <span class="sub-title">Tất cả sản phẩm của Shop</span>
                        <p class="desc">Với mong muốn mang lại sự tin tưởng & hài lòng cho Quý khách mua hàng tại Hublop . Chúng tôi xin cam kết tất cả sản phẩm đều là đồng hồ chính hãng (Authentic) 100% nhập khẩu từ Pháp, Mỹ và các nước Châu âu khác… Tất cả sản bán ra đều có tem vàng bảo đảm và chúng tôi mong muốn mang đến cho Quý khách một dịch vụ hài lòng và những sản phẩm vừa ý nhất.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="aboutus-box-score equal-elem ">
                        <b class="box-score-title">100%</b>
                        <span class="sub-title">Khách hàng hài lòng với sản phẩm</span>
                        <p class="desc">Chúng tôi có niềm tin vào tính gần gũi mà thanh liêm, nền tảng vững chắc cho một cuộc sống trung thực, bình dân và thành thật với bản thân. Chúng tôi dễ gần, đáng yêu và tràn đầy năng lượng, luôn mang đến niềm vui cho những người xung quanh.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="aboutus-box-score equal-elem ">
                        <b class="box-score-title">2 triệu</b>
                        <span class="sub-title">Người dùng tham gia website</span>
                        <p class="desc">Tháng 9/2020, Ví MoMo cán mốc 2 triệu người dùng sau 1 tháng thương hiệu đồng hồ Hublop ra mắt, chinh phục người dùng theo đó hàng loạt sự kiện tri ân người</p>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="aboutus-info style-small-left">
                        <b class="box-title">CHÚNG TÔI THỰC SỰ LÀM GÌ?</b>
                        <p class="txt-content">Luôn khiêm tốn với vị thế của mình và tìm cách học hỏi từ thị trường và đối thủ cạnh tranh
                            Chấp nhận rằng chúng tôi không hoàn hảo
                            Làm việc chăm chỉ trước, tận hưởng sau. Dự đoán những thay đổi và lập kế hoạch trước
                            Chấp nhận những thay đổi không lường trước và chủ động trong việc thực thi</p>
                    </div>
                    <div class="aboutus-info style-small-left">
                        <b class="box-title">History of the Company</b>
                        <p class="txt-content">Ra mắt năm 2021, nền tảng thương mại Hublop được xây dựng nhằm cung cấp cho người sử dùng những trải nghiệm dễ dàng, an toàn và nhanh chóng khi mua sắm trực tuyến thông qua hệ thống hỗ trợ thanh toán và vận hành vững mạnh.

                            Chúng tôi có niềm tin mạnh mẽ rằng trải nghiệm mua sắm trực tuyến phải đơn giản, dễ dàng và mang đến cảm xúc vui thích. Niềm tin này truyền cảm hứng và thúc đẩy chúng tôi mỗi ngày tại Hublop.</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="aboutus-info style-small-left">
                        <b class="box-title">Tầm nhìn của chúng tôi</b>
                        <p class="txt-content">Tự định hướng để phát triển, không cần ai thúc đẩy
                            Luôn khẩn trương hoàn thành công việc</p>
                    </div>
     
                </div>

      
            </div>

            <div class="our-team-info">
                <h4 class="title-box">Our teams</h4>
                <div class="our-staff">
                    <div 
                        class="slide-carousel owl-carousel style-nav-1 equal-container" 
                        data-items="5" 
                        data-loop="false" 
                        data-nav="true" 
                        data-dots="false"
                        data-margin="30"
                        data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"4"}}' >

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="Trần Nhật Nam">
                                    <figure><img src="https://scontent.fsgn5-4.fna.fbcdn.net/v/t1.0-1/p320x320/140327358_1589119524605444_5152023498256716138_o.jpg?_nc_cat=102&ccb=1-3&_nc_sid=7206a8&_nc_ohc=_jFoGmzlOEAAX8NwJb8&_nc_ht=scontent.fsgn5-4.fna&tp=6&oh=ee4867ab70ac9095df22097a0b3ec121&oe=60690E60" alt="LEONA"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">Trần Nhật Nam</b>
                                <span class="title">Chủ tịch</span>
                                <p class="desc">CO-Founder Hublop</p>
                            </div>
                        </div>

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="Nguyễn Thành Nam">
                                    <figure><img src="https://scontent.fsgn5-4.fna.fbcdn.net/v/t1.0-1/p320x320/130131259_191481825963835_4408093757463122436_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=7206a8&_nc_ohc=8s4G1dRZHfgAX8tvrND&_nc_ht=scontent.fsgn5-4.fna&tp=6&oh=d9769666a7c39b26fa1a449e6dad2697&oe=60679D2A" alt="LUCIA"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">Nguyễn Thành Nam</b>
                                <span class="title">Quản lý</span>
                                <p class="desc">CO-Founder Hublop</p>
                            </div>
                        </div>

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="NANA">
                                    <figure><img src="{{ asset('assets/images/member-nana.jpg') }}" alt="NANA"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">NANA</b>
                                <span class="title">Marketer</span>
                                <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
                            </div>
                        </div>

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="BRAUM">
                                    <figure><img src="{{ asset('assets/images/member-braum.jpg') }}" alt="BRAUM"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">BRAUM</b>
                                <span class="title">Member</span>
                                <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
                            </div>
                        </div>

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="LUCIA">
                                    <figure><img src="{{ asset('assets/images/member-lucia.jpg') }}" alt="LUCIA"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">LUCIA</b>
                                <span class="title">Manager</span>
                                <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
                            </div>
                        </div>

                        <div class="team-member equal-elem">
                            <div class="media">
                                <a href="#" title="NANA">
                                    <figure><img src="{{ asset('assets/images/member-nana.jpg') }}" alt="NANA"></figure>
                                </a>
                            </div>
                            <div class="info">
                                <b class="name">NANA</b>
                                <span class="title">Marketer</span>
                                <p class="desc">Contrary to popular belief, Lorem Ipsum is not simply random text...</p>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        <!-- </div> -->
        

    </div><!--end container-->

</main>