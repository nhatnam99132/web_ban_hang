<div>
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md 12"> 
                <div class="panel panel-default"> 
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Cập nhật mã giảm giá
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.coupons') }}" class="btn btn-success pull-right">Tất cả mã giảm giá</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent="updateCoupon">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Mã giảm giá</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Tên danh mục" class="form-control input-md" wire:model="code"/>
                                    @error('code') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Loại mã giảm giá</label>
                                <div class="col-md-4">
                                   <select class="form-control" wire:model="type">
                                    <option value="">Lựa chọn</option>
                                       <option value="fixed">Giảm theo tiền</option>
                                       <option value="percent">Giảm theo phần trăm</option>
                                   </select>
                                    @error('type') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Giá trị mã giảm giá</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Giá trị mã giảm giá" class="form-control input-md" wire:model="value"/>
                                    @error('value') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Giá trị giỏ hàng</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Giá trị giỏ hàng" class="form-control input-md" wire:model="cart_value"/>
                                    @error('cart_value') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>