<div>
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md 12"> 
                <div class="panel panel-default"> 
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Chỉnh sửa sản phẩm
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.products') }}" class="btn btn-success pull-right">Tất cả sản phẩm</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal" enctype="multipart/form-data" wire:submit.prevent="updateProduct">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tên sản phẩm</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Tên sản phẩm" class="form-control input-md" wire:model="name" wire:keyup="generateslug"/>
                                    @error('name') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Đường dẫn tĩnh</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Đường dẫn tĩnh" class="form-control input-md" wire:model="slug"/>
                                    @error('slug') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Mô tả ngắn sản phẩm</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea id="short_description" type="text" placeholder="Mô tả ngắn" class="form-control input-md" wire:model="short_description"></textarea>
                                    @error('short_description') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Mô tả sản phẩm</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea rows="4" id="description" type="text" placeholder="Mô tả ngắn" class="form-control input-md" wire:model="description"></textarea>
                                    @error('description') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Giá sản phẩm</label>
                                <div class="col-md-4">
                                    <input type="number" placeholder="Giá sản phẩm" class="form-control input-md" wire:model="regular_price"></textarea>
                                    @error('regular_price') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Giá khuyến mãi</label>
                                <div class="col-md-4">
                                    <input type="number" placeholder="Giá khuyến mãi" class="form-control input-md" wire:model="sale_price"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">SKU</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="SKU" class="form-control input-md" wire:model="SKU"/>
                                    @error('SKU') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group" wire:model="stock_status">
                                <label class="col-md-4 control-label">Tình trạng kho</label>
                                <div class="col-md-4">
                                    <select>
                                        <option value="Còn hàng">Còn hàng</option>
                                        <option value="Hết hàng">Hết hàng</option>
                                    </select>
                                    @error('stock_status') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group" wire:model="featured">
                                <label class="col-md-4 control-label">Nổi bật</label>
                                <div class="col-md-4">
                                    <select>
                                        <option value="0">Có</option>
                                        <option value="1">Không</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Số lượng sản phẩm</label>
                                <div class="col-md-4">
                                    <input type="number" placeholder="Số lượng sản phẩm" class="form-control input-md" wire:model="quantity" />
                                    @error('quantity') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" >Ảnh sản phẩm</label>
                                <div class="col-md-4">
                                    <input type="file" class="input-file" wire:model="newimage"/>
                                    @if($newimage)
                                        <img src="{{ $newimage->temporaryUrl() }}" width="120" />
                                    @else
                                        <img src="{{ asset('assets/images/products') }}/{{ $image }}" width="120" />
                                    @endif
                                    @error('newimage') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Danh mục</label>
                                <div class="col-md-4">
                                    <select class="form-control" required wire:model="category_id">
                                        <option value="" noSelectionOption="true">--- Chọn danh mục ---</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@push('scripts')
    <script>
        tinymce.init({
            selector: '#short_description',
            setup: function(editior){
                editior.on('Change', function(e){
                    tinyMCE.triggerSave();
                    var sd_data = $('#short_description').val();
                    @this.set('short_description', sd_data);
                })
            }
        });

        tinymce.init({
            selector: '#description',
            setup: function(editior){
                editior.on('Change', function(e){
                    tinyMCE.triggerSave();
                    var d_data = $('#description').val();
                    @this.set('description', d_data);
                })
            }
        });
    </script>
@endpush