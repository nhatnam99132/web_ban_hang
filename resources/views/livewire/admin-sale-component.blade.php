<div>
    <div class="container" style="padding:30px 0;">
        <div class="row">
            <div class="col-md 12"> 
                <div class="panel panel-default"> 
                    <div class="panel-heading">
                        Hệ thống khuyến mãi     
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent="updateSale">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Trạng thái</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="status">
                                        <option value="0">Không hoạt động</option>
                                        <option value="1">Hoạt động</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ngày khuyến mãi</label>
                                <div class="col-md-4">
                                    <input type="text" id="sale-date" placeholder="YYYY/MM/DD h:m:s" class="form-control input-md" wire:model="sale_date"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@push('scripts')
    <script>
        $(function(){
            $('#sale-date').datetimepicker({
                format: 'Y-MM-DD h:m:s',
            })
        })
        .on('dp.change', function(ev){
            var data = $('#sale-date').val();
            @this.set('sale_date', data);
        });
    </script>
@endpush