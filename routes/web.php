<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ListController;
use App\Http\LiveWire\HomeComponent;
use App\Http\LiveWire\CartComponent;
use App\Http\LiveWire\ShopComponent;
use App\Http\LiveWire\CheckoutComponent;
use App\Http\LiveWire\ContactComponent;
use App\Http\LiveWire\AboutComponent;
use App\Http\Livewire\CategoryComponent;
use App\Http\Livewire\DetailsComponent;
use App\Http\Livewire\SearchComponent;
use App\Http\LiveWire\UserDashBoardComponent;
use App\Http\LiveWire\AdminDashBoardComponent;
use App\Http\LiveWire\AdminCategoryComponent;
use App\Http\LiveWire\AdminAddCategoryComponent;
use App\Http\LiveWire\AdminAddProductComponent;
use App\Http\LiveWire\AdminProductComponent;
use App\Http\LiveWire\AdminEditProductComponent;
use App\Http\LiveWire\AdminEditCategoryComponent;
use App\Http\LiveWire\AdminHomeSliderComponent;
use App\Http\LiveWire\AdminAddHomeSliderComponent;
use App\Http\LiveWire\AdminEditHomeSliderComponent;
use App\Http\LiveWire\AdminHomeCategoryComponent;
use App\Http\LiveWire\AdminSaleComponent;
use App\Http\LiveWire\WishlistComponent;
use App\Http\LiveWire\AdminCouponsComponent;
use App\Http\LiveWire\AdminAddCouponComponent;
use App\Http\LiveWire\AdminEditCouponComponent;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', HomeComponent::class);

Route::get('/shop', ShopComponent::class)->name('shop.home');

Route::get('/cart', CartComponent::class)->name('product.cart');

Route::get('/checkout', CheckoutComponent::class);

Route::get('/about', AboutComponent::class);

Route::get('/contact', ContactComponent::class);

Route::get('/product/{slug}', DetailsComponent::class)->name('product.details');

Route::get('/product-category/{category_slug}', CategoryComponent::class)->name('product.category');

Route::get('/search', SearchComponent::class)->name('product.search');

Route::get('/wishlist', WishlistComponent::class)->name('product.wishlist');
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/home', HomeComponent::class);

    Route::get('/user/dashboard', UserDashBoardComponent::class)->name('user.dashboard');   

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    
});

Route::middleware(['auth:sanctum', 'verified', 'authadmin'])->group(function () {
    Route::get('/home', HomeComponent::class);

    Route::get('/admin/dashboard', AdminDashBoardComponent::class)->name('admin.dashboard');  

    Route::get('/admin/categories', AdminCategoryComponent::class)->name('admin.categories');  

    Route::get('/admin/category/add', AdminAddCategoryComponent::class)->name('admin.addcategory');

    Route::get('/admin/category/edit/{category_slug}', AdminEditCategoryComponent::class)->name('admin.editcategory');

    Route::get('/admin/products', AdminProductComponent::class)->name('admin.products');

    Route::get('/admin/product/edit/{product_slug}', AdminEditProductComponent::class)->name('admin.editproduct');

    Route::get('/admin/product/add', AdminAddProductComponent::class)->name('admin.addproduct');

    Route::get('/admin/slider', AdminHomeSliderComponent::class)->name('admin.homeslider');

    Route::get('/admin/slider/edit/{slider_id}', AdminEditHomeSliderComponent::class)->name('admin.edithomeslider');

    Route::get('/admin/slider/add', AdminAddHomeSliderComponent::class)->name('admin.addhomeslider');

    Route::get('admin/home-categories', AdminHomeCategoryComponent::class)->name('admin.homecategories');

    Route::get('/admin/sale', AdminSaleComponent::class)->name('admin.sale');
    
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/admin/coupons', AdminCouponsComponent::class)->name('admin.coupons');

    Route::get('/admin/coupon/edit/{coupon_id}', AdminEditCouponComponent::class)->name('admin.editcoupon');

    Route::get('/admin/coupon/add', AdminAddCouponComponent::class)->name('admin.addcoupon');
    
});

// Route::middleware(['auth:sanctum', 'verified'])->get('/home', function () {
//     return view('dashboard');
// })->name('home');

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// Route::get('/email/verify', function () {
//     return view('auth.verify-email');
// })->middleware('auth')->name('verification.notice');

// Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
//     $request->fulfill();

//     return redirect('/home');
// })->middleware(['auth', 'signed'])->name('verification.verify');

// Route::post('/email/verification-notification', function (Request $request) {
//     $request->user()->sendEmailVerificationNotification();

//     return back()->with('message', 'Verification link sent!');
// })->middleware(['auth', 'throttle:6,1'])->name('verification.send');

// Route::get('admin/login', function() {
//     return view('admin.login');
// });

// Route::middleware(['admin'])->group(function () {
//     Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
//     Route::get('/admin/statistics', [AdminController::class, 'statistics'])->name('admin.statistics');
//     Route::get('/admin/list/{model}', [ListController::class, 'index'])->name('list.index');
//     Route::get('/admin/logout', [AdminController::class, 'logout'])->name('admin.logout');    
// });
// Route::post('/admin/login', [AdminController::class, 'login'])->name('admin.login');
